# Curso 2020/2021
# Evaluación y Rendimiento de Sistemas Software

## Práctica de Laboratorio: Análisis del rendimiento de un servidor web

### Autores
* Alonso Pérez, Álvaro
* Escudero Cuesta, Julio 
* Rodríguez Carracedo, Raúl 

### Descripción
Análisis del rendimiento de un servidor web para el desarrollo de una práctica para la asignatura de ERSS de la universidad de Valladolid.
Los datos a analizar son generados utilizando Jmeter, estos datos se encuentran contenidos dentro de los archivos con extensión '.csv' o '.jtl', los ficheros son tratados a través un notebook de Jupyter utilizando python, estos archivos tienen la extensión '.ipynb'.